const express = require("express");
const { listenServer,
    devuelveAutores,
    creaAutor,
    getAutor,
    deleteAutor,
    putAutor,
    getLibros,
    postLibro,
    getLibro,
    putLibro,
    deleteLibro,
    middleAutores,
    middleLibros
} = require("./functions.js");

const app = express();

app.use(express.json());

app.get("/autores", devuelveAutores);
app.post("/autores", creaAutor);

app.use("/autores/:id/libros", middleAutores);

app.get("/autores/:id", getAutor);
app.delete("/autores/:id", deleteAutor);
app.put("/autores/:id", putAutor);

app.get("/autores/:id/libros", getLibros);
app.post("/autores/:id/libros", postLibro);

app.use("/autores/:id/libros/:idLibro", middleLibros);

app.get("/autores/:id/libros/:idLibro", getLibro);
app.put("/autores/:id/libros/:idLibro", putLibro);
app.delete("/autores/:id/libros/:idLibro", deleteLibro);

app.listen(3000, listenServer);
