let autores = [
    {
        "id": 1,
        "nombre": "Jorge Luis",
        "apellido": "Borges",
        "fechaDeNacimiento": "24/08/1899",
        "libros": [
            {
                "id": 1,
                "titulo": "Ficciones",
                "descripcion": "Es su mejor libro",
                "anioPublicacion": 1944
            },
            {
                "id": 2,
                "titulo": "El Aleph",
                "descripcion": "Es su peor libro",
                "anioPublicacion": 1949
            }
        ]
    }
]

function listenServer() {
    console.log("Server working");
}

function middleAutores(req, res, next) {
    let autorId = Number(req.params.id);
    for (autor of autores) {
        if (autor.id === autorId) {
            return next();
        }
    }
    res.status(404).send("Autor no encontrado");
}

function middleLibros(req, res, next) {
    let autorId = Number(req.params.id);
    let libroId = Number(req.params.idLibro);
    for (autor of autores) {
        if (autor.id === autorId) {
            for (libro of autor.libros) {
                if (libro.id === libroId) {
                    return next();
                }
            }
            res.status(404).send("El libro no corresponde al autor");
        }
    }
    res.status(404).send("Autor no encontrado");
}

function devuelveAutores(req, res) {
    res.json(autores);
}

function creaAutor(req, res) {
    let nuevoAutor = req.body;
    autores.push(nuevoAutor);
    let string = `${nuevoAutor.nombre} ${nuevoAutor.apellido} ha sido agregado con éxito.`
    res.json(string);
}

function getAutor(req, res) {
    const autorId = Number(req.params.id);
    for (autor of autores) {
        if (autor.id === autorId) {
            return res.json(autor);
        }
    }
}

function putAutor(req, res) {
    const autorId = Number(req.params.id);
    for (autor of autores) {
        if (autor.id === autorId) {
            autor.nombre = req.body.nombre;
            autor.apellido = req.body.apellido;
            autor.fechaDeNacimiento = req.body.fechaDeNacimiento;
            autor.libros = req.body.libros;
            let string = `${autor.nombre} ${autor.apellido} ha sido modificado con éxito.`
            return res.json(string);
        }
    }
}

function deleteAutor(req, res) {
    const autorId = Number(req.params.id);
    for (autor of autores) {
        if (autor.id === autorId) {
            let autorIndex = autores.indexOf(autor);
            if (autorIndex > -1) {
                autores.splice(autorIndex, 1);
            }
            let string = `${autor.nombre} ${autor.apellido} ha sido eliminado`;
            return res.json(string);
        }
    }
}

function getLibros(req, res) {
    const autorId = Number(req.params.id);
    for (autor of autores) {
        if (autor.id === autorId) {
            return res.json(autor.libros);
        }
    }
}

function postLibro(req, res) {
    const autorId = Number(req.params.id);
    for (autor of autores) {
        if (autor.id === autorId) {
            let nuevoLibro = req.body;
            autor.libros.push(nuevoLibro);
            let string = `${nuevoLibro.titulo} ha sido agregado con éxito.`;
            res.json(string);
        }
    }
}

function getLibro(req, res) {
    const autorId = Number(req.params.id);
    const idLibro = Number(req.params.idLibro);
    for (autor of autores) {
        if (autor.id === autorId) {
            for (libro of autor.libros) {
                if (libro.id === idLibro) {
                    res.json(libro);
                }
            }
        }
    }
}

function putLibro(req, res) {
    const autorId = Number(req.params.id);
    const idLibro = Number(req.params.idLibro);
    for (autor of autores) {
        if (autor.id === autorId) {
            for (libro of autor.libros) {
                if (libro.id === idLibro) {
                    libro.titulo = req.body.titulo;
                    libro.descripcion = req.body.descripcion;
                    libro.anioPublicacion = req.body.anioPublicacion;
                    let string = `${libro.titulo} ha sido modificado con éxito.`;
                    return res.json(string);
                }
            }
        }
    }
}

function deleteLibro(req, res) {
    const autorId = Number(req.params.id);
    const idLibro = Number(req.params.idLibro);
    for (autor of autores) {
        if (autor.id === autorId) {
            for (libro of autor.libros) {
                if (libro.id === idLibro) {
                    let libroIndex = autor.libros.indexOf(libro);
                    if (libroIndex > -1) {
                        autor.libros.splice(libroIndex, 1);
                    }
                    let string = `${libro.titulo} ha sido eliminado`;
                    return res.json(string);
                }
            }
        }
    }
}

module.exports = {
    listenServer,
    middleAutores,
    devuelveAutores,
    creaAutor,
    getAutor,
    deleteAutor,
    putAutor,
    getLibros,
    postLibro,
    getLibro,
    putLibro,
    deleteLibro,
    middleLibros
}